let x=0; y=0;  //Añado variables glabales como contadores de las partidas ganadas
class Triqui {
    constructor() {
        this.matriz = [
            ["A", "B", "C"],
            ["D", "E", "F"],
            ["G", "H", "I"]
        ];
        this.dibujarTablero();
        this.definirJugador();
        this.realizarJugada(); // se añade realizarJugada com metodo de la clase triqui
    }

    dibujarTablero() {
        let miTablero = document.getElementById("tablero");
        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML +
                "<input type='text' id='casilla" + (i + 1) + "' class='casilla' onclick='miTriqui.realizarJugada()'>";
            if ((i + 1) % 3 == 0) {
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }
        }
    }

    definirJugador() {
        let n;
        let miTurno = document.getElementById("turno");
        n = Math.round(Math.random() + 1);
        if (n === 1) {
            this.turno = "X";
        } else {
            this.turno = "O";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    realizarJugada() {
        let miElemento = event.target;

        if (!(miElemento.value === "X" || miElemento.value === "O")) {
            miElemento.value = this.turno;
            this.modificarMatriz(miElemento.id);

            let resultado=this.verificarTriqui();
            
            if(resultado===true){
                console.warn("Hubo triqui!!");
                alert("Ha ganado: " + this.turno);   //mostrar en alerta el ganador de la partida
                let marcador = document.getElementById("marcador"); // para imprimir en el documento el marcador
                if(this.turno ==="X"){
                    x++;
                    marcador.innerHTML= "El marcador es: x: " + x + " O: " + y;
                }else{
                    y++
                    marcador.innerHTML= "El marcador es: x: " + x + " O: " + y; 
                }
                resultado=false;   //se vuelve al valor de falso
                this.volverAjugar();  // reiniciar juego
            }else{
                this.cambiarTurno();
            }
            
        } else {
            console.error("La casila está llena!!");
            alert("La casilla esta llena!!")
    }
            
        let k=0;    //contador para saber cuando ha habido un empate
            for(let i=1;i<10;i++){
                let casilla=document.getElementById("casilla"+ i);
                if(casilla.value==="X" || casilla.value==="O"){
                    k=k+1
                }
            } 
            if(k===9){
                alert("¡Empate!");  //mostrar en pantalla que ha habido empate
                console.warn("Hubo empate!");
                k=0;
                this.volverAjugar(); //reiniciar juego
            }         
    }

    verificarTriqui() {
        let win = false;
        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][1] === this.matriz[fila][2]) {
                win = true;
                return win;
            }
        }
        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[1][columna] === this.matriz[2][columna]) {
                win = true;
                return win;
            }
        }
        if (this.matriz[0][0] === this.matriz[1][1] && this.matriz[1][1] === this.matriz[2][2]) {
            win = true;
            return win;
        }
        if (this.matriz[0][2] === this.matriz[1][1] && this.matriz[1][1] === this.matriz[2][0]) {
            win = true;
            return win;
        }
    return win;
    }


    modificarMatriz(id) {
        let casillas = [       //se crea matriz para comparar facilmente los valores que puede tomar id
                        ["casilla1","casilla2","casilla3"],
                        ["casilla4","casilla5","casilla6"],
                        ["casilla7","casilla8","casilla9"]
                       ];
        for(let i=0; i<3; i++){  // si el id es casilla: 1,2 o 3 el elemento estara en la posición [0][1,2 o 3]
            if(id === casillas[0][i]){
                    this.matriz[0][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  // si el id es casilla: 4,5 o 6 el elemento estara en la posición [1][1,2 o 3]
            if(id === casillas[1][i]){
                    this.matriz[1][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  // si el id es casilla: 7,8 o 9 el elemento estara en la posición [2][1,2 o 3]
            if(id === casillas[2][i]){
                    this.matriz[2][i] = this.turno;                
            }
        } 
    }

    cambiarTurno(){
        let miTurno = document.getElementById("turno");
        if (this.turno === "X") {
            this.turno = "O";
        } else {
            this.turno = "X";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    volverAjugar(){   //funcion para reiniciar el juego
        for(let i=1;i<10;i++){ //borrar los valores de las casillas
            let casilla=document.getElementById("casilla"+ i);
            casilla.value=" "; 
        }
        this.matriz = [  //devolver valores originales a la matriz
            ["A", "B", "C"],
            ["D", "E", "F"],
            ["G", "H", "I"]
        ];
        this.definirJugador(); //llamar funciones que ejecutan el juego
        this.realizarJugada();
    }
}
 